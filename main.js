// $(document).ready(function(){

// // $('#button').click(function(){

// //   let inputNumber = $('#numbers').val()

// //   for(numbers = 0; numbers <= 1000; numbers++ ){
// //     if(numbers % 10 == inputNumber ){
// //       console.log(numbers)
// //     }
// //   }
// // })
// })

// ----------------soft uni----------------
// --------------------2-------------------

// function solution(input){

//   let numbers = Number(input.shift())
//   let maxNumber = 0
//   let sum = 0

//   for(let number = 1; number <= numbers; number++ ){

//     let currentNumber = Number(input.shift())
//     sum = sum + currentNumber

//     if (currentNumber > maxNumber){
//       maxNumber = currentNumber
//     }
//   }
//     let result = sum - maxNumber
//     console.log(sum)
//     console.log(maxNumber)

//   if(result === maxNumber){
//     console.log('yes')
//     console.log(`sum = ${result}`)
//   } else {
//     let difference = Math.abs(result - maxNumber)
//     console.log('no')
//     console.log(`sum = ${difference}`)
//   }
// }
//   solution([
//     "3",
//     "1",
//     "2",
//     "3",
//   ])
// -----------------3-------------------

// function solution(input) {
//   let numbers = Number(input.shift())

//   let oddSum = 0
//   let oddMin = Number.MAX_SAFE_INTEGER
//   let oddMax = Number.MIN_SAFE_INTEGER
//   let evenSum = 0
//   let evenMin = Number.MAX_SAFE_INTEGER
//   let evenMax = Number.MIN_SAFE_INTEGER


//   for (let position = 1; position <= numbers; position++) {
//     let currentNumber = Number(input.shift())

//     if (position % 2 === 1) {
//       oddSum += currentNumber
//     }
//     if (currentNumber < oddMin) {
//       oddMin = currentNumber
//     }
//     if (currentNumber > oddMax) {
//       oddMax = currentNumber
//     }
//     else {
//       evenSum += currentNumber
//     }
//     if (currentNumber < evenMin) {
//       evenMin = currentNumber
//     }
//     if (currentNumber > evenMax) {
//       evenMax = currentNumber
//     }
//   }
// console.log(`OddSum = ${oddSum.toFixed(2)},`)
// console.log(`OddMin = ${oddMin.toFixed(2)},`)
// console.log(`OddMax = ${oddMax.toFixed(2)},`)
// console.log(`EvenSum = ${evenSum.toFixed(2)},`)
// console.log(`EvenMin = ${evenMin.toFixed(2)},`)
// console.log(`EvenMax = ${evenMax.toFixed(2)}`)
// }

// solution([
//   "6",
//   "2",
//   "3",
//   "5",
//   "4",
//   "2",
//   "1",
// ])
// ----------------------4----------------------

// function solution(input) {
//   let numbers = Number(input.shift())

//   let p1 = 0 // < 200
//   let p2 = 0 // 200 - 399
//   let p3 = 0 // 400 - 599
//   let p4 = 0 // 600 - 799
//   let p5 = 0 // 800 >=

//   for (let n = 1; n <= numbers; n++) {
//     let currentNumber = Number(input.shift())

//     if(currentNumber < 200){
//       p1++
//     } 
//     else if (currentNumber >= 200 && currentNumber <=399){
//       p2++
//     }
//      else if (currentNumber >= 400 && currentNumber <=599){
//       p3++
//     }
//      else if (currentNumber >= 600 && currentNumber <=799){
//       p4++
//     } else {
//       p5++
//     }
//   }

//   let p1percent = p1 / numbers * 100
//   let p2percent = p2 / numbers * 100
//   let p3percent = p3 / numbers * 100
//   let p4percent = p4 / numbers * 100
//   let p5percent = p5 / numbers * 100

//   console.log(`${p1percent.toFixed(2)}%`)
//   console.log(`${p2percent.toFixed(2)}%`)
//   console.log(`${p3percent.toFixed(2)}%`)
//   console.log(`${p4percent.toFixed(2)}%`)
//   console.log(`${p5percent.toFixed(2)}%`)


// }

// solution([
//   "7",
//   "800",
//   "801",
//   "250",
//   "199",
//   "399",
//   "599",
//   "799",
// ])
// -------------------5---------------------

// function solution(input) {
//   let numbers = Number(input.shift())
//   let p1 = 0 // %2
//   let p2 = 0 // %3
//   let p3 = 0 // %4

//   for (let n = 1; n <= numbers; n++) {
//     let currentNumber = Number(input.shift())

//     if (currentNumber % 2 === 0) {
//       p1++
//     } else if (currentNumber % 3 === 0) {
//       p2++
//     } else if (currentNumber % 3 === 0) {
//       p3++
//     }
//   }

//   let p1percent = p1 / numbers * 100
//   let p2percent = p2 / numbers * 100
//   let p3percent = p3 / numbers * 100

//   console.log(p1percent.toFixed(2))
//   console.log(p2percent.toFixed(2))
//   console.log(p3percent.toFixed(2))
// }

// solution([
//   "3",
//   "3",
//   "6",
//   "9",
// ])
// ---------------------6---------------------

function solution(input) {

  let openTabs = Number(input.shift())
  let salary = Number(input.shift())


  for (let i = 1; i <= openTabs; i++) {
    
    let currentSite = input.shift()

    if(salary <= 0){
      console.log('You have lost your salary')
      break
    }
    switch (currentSite) {
      case "Facebook":
        salary -= 150
        break
      case "Instagram":
        salary -= 100
        break
      case "Reddit":
        salary -= 50
        break
    }
  }
  if(salary > 0){
    console.log(salary)
  }
}
solution([
  
    // "10",
    // "750",
    // "Facebook",
    // "Dev.bg",
    // "Instagram",
    // "Facebook",
    // "Reddit",
    // "Facebook",
    // "Facebook",

  "3",
  "500",
  "Github.com",
  "Instagram",
  "softuni.bg",

])